# Configuration de kubeadm

La façon la plus simple est d'installer docker et kubernetes sur une machine puis de la cloner.

### Pour installer docker

``apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common``

``curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -``

``echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list``

``apt update``

``apt-get install docker-ce``

```usermod -aG docker $USER``


### Pour installer kubernetes

``curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -``

``echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list``

``apt update``

``apt-get install kubelet kubeadm kubectl``

Il faut mainteant avoir trois machines avec docker et kubernetes d'installer

### Configuration de la machine master

``kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.168.72.101``

L'ip pod-network-cidr est libre mais la seconde ip correspond à l'ip de la carte réseau qui fournit internet à la vm. A la fin du retour de la commande kubeadm init noter la ligne qui indique comment permettre aux machines slaves de rejoindre le master.

``mkdir -p $HOME/.kube``

``cp -i /etc/kubernetes/admin.conf $HOME/.kube/config``

``chown $(id -u):$(id -g) $HOME/.kube/config``

### Configuration du réseau avec flannel

``echo "net.bridge.bridge-nf-call-iptables=1" >> /etc/sysctl.conf``

``sysctl -p``

``kubectl apply -f https://raw.githubusercontent.com/flannel-io/flannel/master/Documentation/kube-flannel.yml``

Il va désormais falloir configurer certains composant afin de minimiser le nombre de soucis lors de la connexion des slaves avec le master

Commencer par supprimer toutes les iptables sur le master

``iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT``

Désactiver le swap sur toutes les machines

``sudo swapoff -a`

Reconfigurer ensuite le daemon docker sur toutes les machines

``sudo mkdir /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF``

``sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker``

Les machines slave peuvent désormais se connecter au master avec la commande récupérée à la fin du la commande kubeadm-init.

``kubeadm join 192.168.72.101:6443 --token ******* --discovery-token-ca-cert-hash sha256:*********``

Attendre quelques minutes et les noeuds devraient être intégrés au cluster.

Pour vérifier

``kubectl get nodes``

![k8s1](img/k8s1.PNG)

# Installation de kubens et kubectx

On commence par installer krew

``(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.tar.gz" &&
  tar zxvf krew.tar.gz &&
  KREW=./krew-"${OS}_${ARCH}" &&
  "$KREW" install krew
)``

``export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"``

Une fois installer on peux venir installer nos plugins

``kubectl krew install ns``

``kubectl krew install ctx``

# Mise en place d'un wordpress dans k8s

Commencer par créer deux dossiers, un pour wordpress et un pour mysql

``mkdir /home/expand/mysql``

``mkdir /home/expand/wp``

Créer ensuite un namespace dédié pour le projet

``kubectl create namespace prod``

Ensuite il faut venir créer deux pv

Pour cela créer deux fichier .yaml

``vim pv-mysql.yaml``

![Wordpress1](img/wp1.PNG)

Explication du fichier:

Ligne 2 on vient définir un type ici nous avons besoin de créer un Volume persistent par conséquent le type sera PersistentVolume (PV).

Ligne 3 dans la partie metadata on vient renseigné un nom pour le PV puis le namespace que l'on a créer plutot.

Ligne 7 la label local est facultatif il vient nous préciser que l'on travaille en local, il existe plusieurs type par exemple m3.medium pour certaines machines AWS.

Ligne 11 on vient déclarer l'espace que le PV prendra sur la machine/serveur.

Ligne 13 on vient définir les droits d'access sur le PV.

Ligne 15 on vient indiquer le pooint de montage vers un dossier présent sur notre machine, ici c'est le dossier que l'on a créer au début.

On vient ensuite en créer un deuxième pour Wordpress la syntaxe reste globalement la même

``vim pv-wp.yaml``

![Wordpress2](img/wp2.PNG)

Il va désormais falloir créer des persistentVolumeClaim (PVC) pour pouvoir utiliser notre PV.

Comme pour le PV on viens créer un PVC mysql et un autre pour wordpress.

``vim pvc-mysql.yaml``

Explication du fichier:

Ligne 2 ici le type à changé pour PersistentVolumeClaim.

Ligne 3 comme pour la création d'un PV on vient renseigner un nom ainsi que le nom du namespace.

Le reste du fichier de configuration est très similaire si ce n'est qu'ici on vient utiliser seulement 3 des 5 Go du PV dans notre PVC. Il y aura don un stockage de 3Go sur le pod qui utilisera le PVC.

On vient ensuite en créer un deuxième pour Wordpress la syntaxe reste globalement la même

``vim pvc-wp.yaml``

Maintenant que le stockage est configuré on peut venir le deployer.

``kubectl apply -f pv-mysql.yaml``
``kubectl apply -f pv-wp.yaml``
``kubectl apply -f pvc-mysql.yaml``
``kubectl apply -f pvc-mysql.yaml``

On va maintenant configurer le déploiement de mysql.

Créer un fichier .yaml

``vim mysql-deployment.yaml``

![Wordpress3](img/wp3.PNG)

Explication du fichier:

De la ligne 1 à 14 on vient créer un service qui va nous permette d'exposer un port mais uniquement pour que notre conteneur wordpress puisse y accéder.
Comme pour les fichiers précédent on y déclare un nom et le namespace dans les metadata.

A la ligne 10 on vient indiquer le port qui doit être accessible.

Puis on appelle le label déclaré ligne 7 dans le selector à la ligne 12.

A partir de la ligne 16 on créer un deploiement.

Le début ressemble aux autres fichiers que l'on a déja créé.

A la ligne 29 on vient indiquer la strategy de déploiement. Ici recreate indique que tous les pods existants sont tués avant que de nouveaux ne soient créés.

A partir de la ligne 30 on viens créer le template de notre pods. On lui déclare des metadata.

A la ligne 36, on vient lui indiquer l'image à utiliser, ici c'est une image mysql en version 5.6.

A la ligne 38 on vient déclarer une variable d'environement. Cette variable fais référence à un "secrets" que nous expliquerons plus tard.

A la ligne 46 on vient renseigné le port utilisé par mysql.

Puis a la fin du fichier on vient connecté notre pvc à notre pod.
Pour cela on commence par définir la partie volumeMounts dans laquel on déclare un nom ainsi que le point de montage dans notre conteneur. Dans la partie volumes on rapelle le nom que l'on a déclaré juste au dessus puis on appelle le nom avec lequel on a nommé notre PVC.

Après avoir créer le fichier pour le mysql on vient faire la même chose pour le wordpress.

``vim wordpress-deployment.yaml``

![Wordpress4](img/wp4.PNG)

Le fichier ressemble beaucoup au précédent sauf sur quelque point.

A la ligne 14 on vient indiquer que le service est un load-balancer. Cela permet d'avoir un port attribué aléatoirement pour accéder au front-end du wordpress et aussi de pouvoir faire du load-balancing si jamais nous avions plusieur replica par exemple.

A la ligne 40 il y a une variable d'environement renseignée en clair la ou juste en dessus on vient encore utilmisé notre "secrets" pour avoir une variable d'environement chiffrée.

Une fois ces deux fichiers créés on vient créer un dernier fichier kustomization.yaml

``vim kustomization.yaml``

![Wordpress4](img/wp5.PNG)

A la ligne 2 on vient déclarer notre secrets utilisé dans les deux fichiers précédents.

Puis on indique les ressources qui pourront être prisent en compte lors de l'application de la configuration du ficher kustomization.

On deploie désormais nos pods.

``kubectl apply -k ./``

Notre wordpress est désormais opérationel.

![final](img/final.PNG)

# Installer minikube
 
```
apt install curl wget apt-transport-https -y
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
cp minikube-linux-amd64 /usr/local/bin/minikube
chmod +x /usr/local/bin/minikube
```

# Installer kubectl

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin
```

# Installer  helm

```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

# Déploiement avec Helm

```
helm repo add bitnami https://charts.bitnami.com/bitnami
wget https://raw.githubusercontent.com/helm/charts/master/stable/wordpress/values.yaml
helm install wordpress bitnami/wordpress -f ./values.yaml
```
 
Pour accéder à votre site WordPress depuis l'extérieur du cluster, suivez les étapes ci-dessous :

1. Obtenez l'URL de WordPress en exécutant ces commandes :

  NOTE : Cela peut prendre quelques minutes pour que l'IP du LoadBalancer soit disponible.
        Surveillez le statut avec : kubectl get svc --namespace default -w wordpress'.
 
```
export SERVICE_IP=$(kubectl get svc --namespace default wordpress --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")
echo "WordPress URL: http://$SERVICE_IP/"
echo "WordPress Admin URL: http://$SERVICE_IP/admin"
```
 
2. Ouvrez un navigateur et accédez à WordPress en utilisant l'URL obtenue.

3. Connectez-vous avec les informations d'identification suivantes pour voir votre blog :

```
echo Nom d'utilisateur : andrei
echo Mot de passe : $(kubectl get secret --namespace default wordpress -o jsonpath="{.data.wordpress-password}" | base64 --decode)
```

Architecture :

![wordpress-helm](img/wordpress-helm.png)
 

# Installation de velero dans un cluster kubeadm

Télécharger le binaire

``curl -LO https://github.com/heptio/velero/releases/download/v1.1.0/velero-v1.1.0-linux-amd64.tar.gz``

Puis le dézipper le dans /usr/local/bin

``tar -C /usr/local/bin -xzvf velero-v1.1.0-linux-amd64.tar.gz``

``export PATH=$PATH:/usr/local/bin/velero-v1.1.0-linux-amd64/``

Utiliser le yaml fournit par vmware pour déployer Minio qui nous servira de stockage.

``git clone https://github.com/heptio/velero``

``kubectl apply -f velero/examples/minio/00-minio-deployment.yaml``

Une fois le déploiment terminé, créer un fichier credentials-velero

``vim credentials-velero``

![velero3](img/velero3.PNG)

Installer ensuite velero en le conectant à minio.

``velero install \
--provider aws \
--plugins velero/velero-plugin-for-aws:v1.0.0 \
--bucket velero \
--secret-file ./credentials-velero \
--use-volume-snapshots=false \
--backup-location-config region=minio,s3ForcePathStyle=”true”,s3Url=http://minio.velero.svc:9000``

Une fois terminé un retour console semblable à celui la devrais s'afficher.

![velero1](img/velero1.PNG)

On peut désormais lancer nos backups avec la commande suivante.


``velero backup create <bacup_name> --include-namespaces <namespace_name>``

![velero1](img/velero2.PNG)
